FROM maven:3.6.3-jdk-11-slim AS builder
WORKDIR /application
COPY pom.xml .
RUN mvn -e -B dependency:resolve
COPY src ./src
RUN mvn -e -B package

FROM openjdk:11-jre-slim
EXPOSE 8080
COPY --from=builder /application/target/kotlin-web-sockets*.jar /app.jar
CMD ["java","-jar","/app.jar"]