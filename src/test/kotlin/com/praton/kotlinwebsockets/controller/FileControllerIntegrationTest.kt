package com.praton.kotlinwebsockets.controller

import com.praton.kotlinwebsockets.model.File
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FileControllerIntegrationTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Test
    fun shouldSearchBooks() {
        val result = testRestTemplate.getForEntity("/files/1",
                File::class.java)
        assertNotNull(result)
        assertEquals(HttpStatus.OK, result.statusCode)
        val file = result.body
        assertEquals("1", file?.id)
        assertEquals("firstFile", file?.name)
    }
}