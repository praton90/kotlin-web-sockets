package com.praton.kotlinwebsockets.controller

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class FileControllerUnitTest {

    @Autowired
    lateinit var fileController: FileController

    @Test
    fun testDemoController() {
        val result = fileController.getFiles()
        Assertions.assertNotNull(result)
        Assertions.assertEquals("firstFile", result.name)
    }
}

