package com.praton.kotlinwebsockets.controller

import com.praton.kotlinwebsockets.model.File
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
@RequestMapping("/files")
class FileController {

    @GetMapping("/1")
    fun getFiles() = File("1", "firstFile", Instant.now(), "http://file-url.com")
}