package com.praton.kotlinwebsockets

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinWebSocketsApplication

fun main(args: Array<String>) {
	runApplication<KotlinWebSocketsApplication>(*args)
}
