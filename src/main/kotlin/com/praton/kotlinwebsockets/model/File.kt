package com.praton.kotlinwebsockets.model

import java.time.Instant

class File(val id: String, val name: String, val createdAt: Instant, val url: String)