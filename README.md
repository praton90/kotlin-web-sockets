# kotlin-web-sockets

Kotlin app with SpringBoot exposing a HTTP endpoint that return a mocked file information.

This application has:
- Dockerfile. To be able to create a docker image for this service.
- Kubernetes config files. Configuration files required to create a namespace, service and deployment for Kubernetes.
- CI/CD. Through BitBucket pipelines. The current steps are: test cases execution, package the project, build and push the docker image. Due to the time constraint I was not able to complete the deploy step, so only CI is covered by the pipeline.

## Getting Started

Please go through the following steps to get this API running locally.

### Prerequisites

- Java 11
- Maven

### How to run it locally?

- Clone the repository.
- Access project directory. `cd kotlin-web-sockets`
- Execute the following command `./mvnw spring-boot:run`

### How to run it inside a docker container?

- Clone the repository.
- Access project directory. `cd kotlin-web-sockets`
- Build docker image `docker build -t <image name> .`
- Run docker image `docker run -d -P <image name>`

The image expose the port required to access the REST API.

## Built With
This service was created using [Spring Initializr](https://start.spring.io/)

* [SpringBoot](https://spring.io/projects/spring-boot) - Spring framework
* [Maven](https://maven.apache.org/) - Dependency Management
* [Docker](https://www.docker.com/) - Create, deploy, and run applications by using containers
* [Kubernetes](https://kubernetes.io/) - Container orchestration system
* [BitBucket Pipeline](https://bitbucket.org/product/en/features/pipelines) - Integrated CI/CD

## TODO's. 

Below are the pending steps sorted by my priority.

- Create deploy step in the pipeline to offer CD.
- Implement Web Socket service to receive binary streams.
- Store the files received through the Web Socket Service in some storage.
- Implement client to send binary streams to the Web Socket Service.
- The REST endpoint should return information about the last file received instead of mocked information.

